import sys

for line in sys.stdin:
    q = int(line)
    s = ''

    while(True):
        q, r = divmod(q, 2)
        s += str(r)

        if q == 0:
            break

    s = s[::-1]

    total = 0
    place = 0
    for x in range(0,len(s)):
        c = int(s[x])
        total += c * 2 ** place
        place += 1

    print(total)
