import sys
alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ_.'
for lnNum, line in enumerate(sys.stdin):
    lineSplit = line.split(' ')
    if lineSplit[0] == '0\n':
        sys.exit(0)

    lineSplit[0],lineSplit[1] = int(lineSplit[0]),lineSplit[1][::-1]
    for letterIndex, letter in enumerate(lineSplit[1]):
        for charNum, char in enumerate(alpha):
            if letter == char:
                index = charNum + lineSplit[0]
                if index > len(alpha) - 1:
                    index -= len(alpha)

                print(alpha[index], end="")
    end = '\n' if lineSplit[0] != '' else ''
    print('', end=end)
