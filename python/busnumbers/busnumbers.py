import sys

num = 0
for line in sys.stdin:
    if num == 1:
        stops = [int(x) for x in line.split(' ')]
        stops.sort()

        if len(stops) == 1:
            print(stops[0])
        else:
            index = -1
            combiner = [0,0]
            toPrint = []
            while index < len(stops)-1:
                index += 1
                curStop = stops[index]

                if index == len(stops)-1:
                    if combiner[0] != 0:
                        combiner[1] = curStop
                        if combiner[0]+1 == combiner[1]:
                            toPrint.append(combiner[0])
                            toPrint.append(combiner[1])
                        else:
                            toPrint.append(str(combiner[0]) + '-' + str(combiner[1]))
                    else:
                        toPrint.append(curStop)

                elif stops[index + 1] == curStop + 1:
                    if combiner[0] == 0:
                        combiner[0] = curStop
                    else:
                        combiner[1] = curStop

                else:
                    if combiner[0] != 0:
                        combiner[1] = curStop
                        if combiner[0]+1 == combiner[1]:
                            toPrint.append(combiner[0])
                            toPrint.append(combiner[1])
                        else:
                            toPrint.append(str(combiner[0]) + '-' + str(combiner[1]))
                        combiner = [0,0]
                    else:
                        toPrint.append(curStop)

            for x in range(0,len(toPrint)):
                if x >= len(toPrint)-1:
                    print(toPrint[x])
                else:
                    print(toPrint[x], end=" ")

    num += 1
