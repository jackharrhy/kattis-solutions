import sys

myMap = {
    "a": "@",
    "b": "8",
    "c": "(",
    "d": "|)",
    "e": "3",
    "f": "#",
    "g": "6",
    "h": "[-]",
    "i": "|",
    "j": "_|",
    "k": "|<",
    "l": "1",
    "m": "[]\/[]",
    "n": "[]\[]",
    "o": "0",
    "p": "|D",
    "q": "(,)",
    "r": "|Z",
    "s": "$",
    "t": "']['",
    "u": "|_|",
    "v": "\/",
    "w": "\/\/",
    "x": "}{",
    "y": "`/",
    "z": "2",
}

for line in sys.stdin:
    string = str(line)

    for s in range(0,len(string)):
        char = string[s].lower()

        if char != '\n':
            if char in myMap:
                print(myMap[char],end="")
            else:
                print(char, end="")
