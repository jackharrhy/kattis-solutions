import sys
t = 0
for ln in sys.stdin:
    num = ln[0:len(ln) - 2]
    if num == '':
        num = 0
    exp = ln[len(ln) - 2:len(ln) - 1]
    t += pow(int(num),int(exp))
print(t)
