import sys
n = -1
for line in sys.stdin:
    n += 1

    if n > 0:
        array = line.split(" ")

        nArray = []
        total = 0
        for x in range(1,len(array)):
            val = int(array[x])
            total += val
            nArray.append(val)

        average = total / len(nArray)

        aboveAverage = 0
        for x in range(0,len(nArray)):
            grade = nArray[x]
            if grade > average:
                aboveAverage += 1

        print('%.3f' % (aboveAverage * (100/len(nArray))),'%', sep="")
