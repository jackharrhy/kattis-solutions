import sys
cur, line = 1, sys.stdin.read()
for letter in line:
    if letter == 'A':
        cur = 2 if cur == 1 else 1 if cur == 2 else 3
    if letter == 'B':
        cur = 3 if cur == 2 else 2 if cur == 3 else 1
    if letter == 'C':
        cur = 1 if cur == 3 else 3 if cur == 1 else 2
print(cur)
