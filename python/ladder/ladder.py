import sys
import math
inputStr = sys.stdin.read()
args = inputStr.split(' ')
args[0], args[1] = int(args[0]), math.radians(int(args[1]))
print(math.ceil(args[0] / math.sin(args[1])))
