import sys
for lnNum, ln in enumerate(sys.stdin):
    if lnNum != 0:
        ln = int(ln.replace('\n', ''))
        text = ' is even' if ln % 2 == 0 else ' is odd'
        print(str(ln) + text)
