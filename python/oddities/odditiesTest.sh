cat $1.in
printf "=\n"
cat $1.ans
printf "?\n"
cat $1.in | python3 oddities.py
printf "\n"
cat $1.in | python3 oddities.py > ./out
diff ./out ./$1.ans
rm ./out
