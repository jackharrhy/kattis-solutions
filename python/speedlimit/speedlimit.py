import sys

total = 0
i = -1
for line in sys.stdin:
    if i <= 0:
        i = int(line)
        if total != 0:
            print(total, "miles")
        total = 0
        prev = 0
    else:
        speed, elapsed = line.split(" ")
        speed, elapsed = int(speed), int(elapsed)

        total += speed * (elapsed - prev)
        prev = elapsed

        i -= 1
