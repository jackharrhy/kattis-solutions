import sys
cur, usedNames = 0, {}
for lnNum, ln in enumerate(sys.stdin):
    try:
        ln = int(ln)
        if len(usedNames) > 0:
            print(len(usedNames))
        cur, usedNames = 0, {}
    except ValueError:
        usedNames[ln] = True
        cur += 1
print(len(usedNames))
