import sys
finalScores, winner = [0] * 5, [0, 0]
for lnNum, ln in enumerate(sys.stdin):
    scores, total = ln.replace('\n', '').split(' '), 0
    for score in scores:
        total += int(score)
    for finalScore in finalScores:
        winner = [lnNum + 1, total] if total > winner[1] else winner
print(str(winner[0]) + ' ' + str(winner[1]))
