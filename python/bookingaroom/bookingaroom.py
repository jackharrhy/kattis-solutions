import sys
import random

amountOfRooms = 0
booked = 0
alreadyBooked = []

num = 0
for line in sys.stdin:
    if num == 0:
        amountOfRooms, booked = line.split(' ')
        amountOfRooms, booked = int(amountOfRooms), int(booked)
        alreadyBooked = [False] * amountOfRooms
    else:
        alreadyBooked[int(line)-1] = True
    num += 1

room = 0
if amountOfRooms == booked:
    print('too late')
else:
    while True:
        room = random.randint(1,amountOfRooms)
        isItBooked = False
        for x in range(0,len(alreadyBooked)):
            if alreadyBooked[room-1]:
                isItBooked = True
        if not isItBooked:
            break
    print(room)
