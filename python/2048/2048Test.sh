if [ $1 ]; then
	cat ./samples/$1.in
	printf "=\n"
	cat ./samples/$1.ans
	printf "?\n"
	cat ./samples/$1.in | python3 2048.py
	printf "\n"
	cat ./samples/$1.in | python3 2048.py > ./out
	diff ./out ./samples/$1.ans
	rm out
else
	echo 'Pass a value from 1 - 6'
fi
