# Is this code horrible?, yes
# Is it only 40 lines?, yes!
import sys
import copy
array = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
preArray, stayArray = copy.deepcopy(array), copy.deepcopy(array)
preArray[0], d, v, lnNum = 1, None, None, -1
for lnNum, ln in enumerate(sys.stdin):
    if lnNum <= 3:
        strArray = ln.split(' ')
        for i, val in enumerate(strArray):
            array[lnNum][i] = int(val.replace('\n', ''))
    else:
        d = int(ln)
        v = [-1, 0] if d == 0 else [0, -1] if d == 1 else [1, 0]
        if d == 3:
            v = [0, 1]
while(preArray != array):
    preArray = copy.deepcopy(array)
    for y, row in enumerate(array):
        for x, val in enumerate(row):
            x = (x * (v[0] * -1)) + 3 if -1 != v[0] != 0 else x
            y = (y * (v[1] * -1)) + 3 if -1 != v[1] != 0 else y
            cVal, mP = array[y][x], [x + v[0], y + v[1]]
            if 0 <= mP[0] <= 3 and 0 <= mP[1] <= 3:
                mVal = array[mP[1]][mP[0]]
                if mVal == 0:
                    array[y][x], array[mP[1]][mP[0]] = 0, cVal
                    if(stayArray[y][x] == 1):
                        stayArray[y][x] == 0
                        stayArray[mP[1]][mP[0]] = 1
                elif (mVal == cVal):
                    if stayArray[mP[1]][mP[0]] == 0 and stayArray[y][x] == 0:
                        array[y][x] = 0
                        array[mP[1]][mP[0]] = cVal + mVal
                        stayArray[mP[1]][mP[0]] = 1
for y, row in enumerate(array):
    print() if y != 0 else print(end="")
    for x, val in enumerate(row):
        end = " " if x != 3 else ""
        print(val, end=end)
print()
