import sys
from copy import deepcopy

array = []

def printState(state):
    for y in range(0,len(state)):
        row = state[y]
        for x in range(0,len(row)):
            cell = row[x]
            if cell:
                print("0",end=" ")
            else:
                print("_",end=" ")
        print()

def findClearRegion(state):
    for y in range(0,len(state)):
        row = state[y]
        for x in range(0,len(row)):
            cell = row[x]
            if not cell:
                return x,y
    return -1,-1

def cellFill(x,y,state,lx,ly):
    if not state[y][x]:
        state[y][x] = True

        if x-1 > 0 and (x-1) != lx:
            state = cellFill(x-1,y,state,x,-1)

        if x+1 < len(state[0]) and (x+1) != lx:
            state = cellFill(x+1,y,state,x,-1)

        if y-1 > 0 and (y-1) != ly:
            state = cellFill(x,y-1,state,-1,y)

        if y+1 < len(state) and (y+1) != ly:
            state = cellFill(x,y+1,state,-1,y)

        return state
    return state

def checkForRegions(state):
    startX = 0
    regions = 0
    while(True):
        startX, startY = findClearRegion(state)
        if startX < 0:
            break
        state = cellFill(startX, startY, state, -1,-1)
        regions += 1

    return regions

i = -1
for line in sys.stdin:
    i += 1

    if i == 0:
        n,m,q = line.split(' ')
        n,m,q = int(n),int(m),int(q)

        array = [[False for i in range(n)] for i in range(m)]
    else:
        x1,y1,x2,y2 = line.split(' ')
        x1,y1,x2,y2 = int(x1),int(y1),int(x2),int(y2)

        if x1 == x2:
            if y1 <= y2:
                for y in range(y1,y2+1):
                    array[y-1][x1-1] = True
        else:
            if x1 <= x2:
                for x in range(x1,x2+1):
                    array[y1-1][x-1] = True

        copy = deepcopy(array)
        regions = checkForRegions(copy)
        print(regions)
