import sys

lines = []
for line in sys.stdin:
    lines.append([int(x) for x in line.strip('\n').split(' ')])

c, p = lines[0]
base = lines[1]

checks = [
        [[0,0,0]], # 1
        [[0]], # 2
        [[-1],[0,1]], # 3
        [[-1,0],[1]], # 4
        [[0,0],[-1,1],[1],[-1]], # 5
        [[0,0],[-2],[0]], # 6
        [[0,0],[-2],[0]]  # 7
        ]

# if 1 always at least 1
print(c, p)
print(base)

passes = 0
if p == 1:
    passes = 1

checking = checks[p-1]

for x in range(c):
    for check in checking:
        head = x
        valid = True
        for y in range(len(check)):
            if len(base) < head-2:
                if base[head] + check[y] != base[head+1]:
                    valid = False
                head += 1
        if valid:
            passes += 1

print(passes)

