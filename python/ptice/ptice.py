import sys
Adrian = ['A', 'B', 'C']
Bruno =  ['B', 'A', 'B', 'C']
Goran = ['C', 'C', 'A', 'A', 'B', 'B']
people, score = [Adrian,Bruno,Goran], [0,0,0]
answers = None
for line in sys.stdin:
    answers = line.replace('\n', '')
for personNum, person in enumerate(people):
    while(len(person) < len(answers)):
        person *= 2
    for charNum, char in enumerate(answers):
        if person[charNum] == char:
            score[personNum] += 1
greatest, winners = 0, ''
for pointNum, points in enumerate(score):
    if points > greatest:
        greatest = points
        winners = 'Adrian\n' if pointNum == 0 else 'Bruno\n' if pointNum == 1 else 'Goran'
    elif points == greatest:
        winners += 'Adrian\n' if pointNum == 0 else 'Bruno\n' if pointNum == 1 else 'Goran'
print(str(greatest) + '\n' + str(winners))
