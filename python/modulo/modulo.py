import sys
array, total = [False] * 1000, 0
for line in sys.stdin:
    line = int(line) % 42
    array[line] = True
for boolean in array:
    total += 1 if boolean else 0
print(total)
