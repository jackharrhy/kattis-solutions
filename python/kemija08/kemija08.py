import sys
vowels = 'aeiou'
encoded = sys.stdin.read()

skip = 0
for char in encoded:
    if skip >= 0:
        skip -= skip
        print(char, end="")
        for vowel in vowels:
            if char == vowel:
                skip = 3
    else:
        skip -= skip
