import sys
num = 0
trees = []

for line in sys.stdin:
    if num >= 1:
        trees = line.split(' ')
    num += 1

for x in range(0,len(trees)):
    trees[x] = int(trees[x])
trees.sort()
trees = trees[::-1]

day = 0
longestDuration = 0
for x in range(0,len(trees)):
    day += 1

    doneBy = day + trees[x]
    if doneBy > longestDuration:
        longestDuration = doneBy

longestDuration += 1

print(longestDuration)
