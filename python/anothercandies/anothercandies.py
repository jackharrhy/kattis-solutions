import sys
string = ""
for line in sys.stdin:
    string += ' ' if line == '\n' else line.replace('\n', '')
sections, cur = string.split(' '), 1
while(cur <= int(sections[0])):
    total = 0
    for string in sections[cur]:
        total += int(string)
    print('YES') if total % 2 == 0 else print('NO')
    cur += 1
