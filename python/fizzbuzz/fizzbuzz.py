import sys

for line in sys.stdin:
    x,y,n = line.split(' ')
    x,y,n = int(x), int(y), int(n)

    for a in range(1,n+1):
        if a % x == 0:
            print("Fizz", end="")
        if a % y == 0:
            print("Buzz", end="")
        if a % x != 0 and a % y != 0:
            print(a, end="")
        print()
