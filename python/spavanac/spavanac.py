import sys

for line in sys.stdin:
    h,m = line.split(' ')
    h,m = int(h),int(m)

    ts = h * 3600 + m * 60

    nt = ts - 45 * 60

    if nt < 0:
        nt = (24 * 3600) + nt

    nh, nm = divmod(nt, 3600)
    nm //= 60

    print(nh,nm)

