import sys

for line in sys.stdin:
    num, denom = line.split(' ')
    num, denom = int(num), int(denom)

    if num != 0 and denom != 0:
        whole, part = divmod(num, denom)
        print(whole, part, '/', denom)
